from collections import OrderedDict


months = dict() 

months['January'] = 1
months['February'] = 2
months['March'] = 3
months['April'] = 4
months['May'] = 5
months['June'] = 6

print(months.items())

ordered_months = OrderedDict()
ordered_months['January'] = 1
ordered_months['February'] = 2
ordered_months['March'] = 3
ordered_months['April'] = 4
ordered_months['May'] = 5
ordered_months['June'] = 6

print(ordered_months.items())
