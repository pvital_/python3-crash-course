# What’s New In [Python 3.1]

## [PEP 372]: Ordered Dictionaries

Regular Python dictionaries iterate over key/value pairs in arbitrary order. To remember the order 
that the keys were originally inserted, a new ```collections.OrderedDict``` class has been 
introduced.

The OrderedDict API is substantially the same as regular dictionaries but will iterate over keys
and values in a guaranteed order depending on when a key was first inserted. If a new entry
overwrites an existing entry, the original insertion position is left unchanged. Deleting an entry
and reinserting it will move it to the end.

## [PEP 378]: Format Specifier for Thousands Separator

The built-in ```format()``` function and the ```str.format()``` method use a mini-language that now
includes a simple, non-locale aware way to format a number with a thousands separator:

```python
>>>
>>> format(1234567, ',d')
'1,234,567'
>>> format(1234567.89, ',.2f')
'1,234,567.89'
>>> format(12345.6 + 8901234.12j, ',f')
'12,345.600000+8,901,234.120000j'
>>> format(Decimal('1234567.89'), ',f')
'1,234,567.89'
```

The supported types are ```int```, ```float```, ```complex``` and ```decimal.Decimal```.

## Other smaller changes

- The ```int()``` type gained a ```bit_length``` method that returns the number of bits necessary
to represent its argument in binary:

```python
>>> n = 37
>>> bin(37)
'0b100101'
>>> n.bit_length()
6
>>> n = 2**123-1
>>> n.bit_length()
123
>>> (n+1).bit_length()
124
```

- The syntax of the ```with``` statement now allows multiple context managers in a single statement:

```python
>>>
>>> with open('mylog.txt') as infile, open('a.out', 'w') as outfile:
...     for line in infile:
...         if '<critical>' in line:
...             outfile.write(line)
```

With the new syntax, the ```contextlib.nested()``` function is no longer needed and is now 
deprecated.

- ```round(x, n)``` now returns an integer if *x* is an integer. Previously it returned a float:

```python
>>>
>>> round(1123, -2)
1100
```

- Python now uses David Gay’s algorithm for finding the shortest floating point representation that
doesn’t change its value. This should help mitigate some of the confusion surrounding binary
floating point numbers.


[Python 3.1]: https://docs.python.org/3/whatsnew/3.1.html
[PEP 372]: https://www.python.org/dev/peps/pep-0372/
[PEP 378]: https://www.python.org/dev/peps/pep-0378/
