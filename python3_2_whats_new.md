# What’s New In [Python 3.2]

## [PEP 384]: Defining a Stable ABI

In the past, extension modules built for one Python version were often not usable with other Python
versions. With Python 3.2, an alternative approach becomes available: extension modules which
restrict themselves to a limited API (by defining Py_LIMITED_API) cannot use many of the internals,
but are constrained to a set of API functions that are promised to be stable for several releases.

As a consequence, extension modules built for 3.2 in that mode will also work with 3.3, 3.4, and 
so on.

## [PEP 389]: Argparse Command Line Parsing Module

A new module for command line parsing, ```argparse```, was introduced to overcome the limitations 
of ```optparse``` which did not provide support for positional arguments (not just options), 
subcommands, required options and other common patterns of specifying and validating options.

Here’s an annotated example parser showing features like limiting results to a set of choices, 
specifying a metavar in the help screen, validating that one or more positional arguments is 
present, and making a required option:

```python
import argparse
parser = argparse.ArgumentParser(
            description = 'Manage servers',         # main description for help
            epilog = 'Tested on Solaris and Linux') # displayed after help
parser.add_argument('action',                       # argument name
            choices = ['deploy', 'start', 'stop'],  # three allowed values
            help = 'action on each target')         # help msg
parser.add_argument('targets',
            metavar = 'HOSTNAME',                   # var name used in help msg
            nargs = '+',                            # require one or more targets
            help = 'url for target machines')       # help msg explanation
parser.add_argument('-u', '--user',                 # -u or --user option
            required = True,                        # make it a required argument
            help = 'login as user')
```

Example of calling the parser on a command string:

```python
>>>
>>> cmd = 'deploy sneezy.example.com sleepy.example.com -u skycaptain'
>>> result = parser.parse_args(cmd.split())
>>> result.action
'deploy'
>>> result.targets
['sneezy.example.com', 'sleepy.example.com']
>>> result.user
'skycaptain'
```

Example of the parser’s automatically generated help:

```python
>>>
>>> parser.parse_args('-h'.split())

usage: manage_cloud.py [-h] -u USER
                       {deploy,start,stop} HOSTNAME [HOSTNAME ...]

Manage servers

positional arguments:
  {deploy,start,stop}   action on each target
  HOSTNAME              url for target machines

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  login as user

Tested on Solaris and Linux
```

An especially nice argparse feature is the ability to define subparsers, each with their own 
argument patterns and help displays:

```python
import argparse
parser = argparse.ArgumentParser(prog='HELM')
subparsers = parser.add_subparsers()

parser_l = subparsers.add_parser('launch', help='Launch Control')   # first subgroup
parser_l.add_argument('-m', '--missiles', action='store_true')
parser_l.add_argument('-t', '--torpedos', action='store_true')

parser_m = subparsers.add_parser('move', help='Move Vessel',        # second subgroup
                                 aliases=('steer', 'turn'))         # equivalent names
parser_m.add_argument('-c', '--course', type=int, required=True)
parser_m.add_argument('-s', '--speed', type=int, default=0)
```

```bash
$ ./helm.py --help                         # top level help (launch and move)
$ ./helm.py launch --help                  # help for launch options
$ ./helm.py launch --missiles              # set missiles=True and torpedos=False
$ ./helm.py steer --course 180 --speed 5   # set movement parameters
```

## [PEP 391]: Dictionary Based Configuration for Logging

The ```logging``` module provided two kinds of configuration, one style with function calls for 
each option or another style driven by an external file saved in a ConfigParser format. Those 
options did not provide the flexibility to create configurations from JSON or YAML files, nor did 
they support incremental configuration, which is needed for specifying logger options from a 
command line.

To support a more flexible style, the module now offers ```logging.config.dictConfig()``` for 
specifying logging configuration with plain Python dictionaries. The configuration options include 
formatters, handlers, filters, and loggers. Here’s a working example of a configuration dictionary:

```python
{
    "version": 1,
    "formatters": {
        "brief": {"format": "%(levelname)-8s: %(name)-15s: %(message)s"},
        "full": {"format": "%(asctime)s %(name)-15s %(levelname)-8s %(message)s"}
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "brief",
            "level": "INFO",
            "stream": "ext://sys.stdout"
        },
        "console_priority": {
            "class": "logging.StreamHandler",
            "formatter": "full",
            "level": "ERROR",
            "stream": "ext://sys.stderr"
        }
    },
    "root": {
        "level": "DEBUG", 
        "handlers": ["console", "console_priority"]
    }
}
```

If that dictionary is stored in a file called ```conf.json```, it can be loaded and called with 
code like this:

```python
>>>
>>> import json, logging.config
>>> with open('conf.json') as f:
...     conf = json.load(f)
...
>>> logging.config.dictConfig(conf)
>>> logging.info("Transaction completed normally")
INFO    : root           : Transaction completed normally
>>> logging.critical("Abnormal termination")
2011-02-17 11:14:36,694 root            CRITICAL Abnormal termination
```

## Other Language Changes

- The ```hasattr()``` function works by calling ```getattr()``` and detecting whether an exception
is raised. This technique allows it to detect methods created dynamically by ```__getattr__()``` or 
```__getattribute__()``` which would otherwise be absent from the class dictionary. 

```python
>>>
>>> class A:
...     @property
...     def f(self):
...         return 1 // 0
...
>>> a = A()
>>> hasattr(a, 'f')
Traceback (most recent call last):
  ...
ZeroDivisionError: integer division or modulo by zero
```

- ```range``` objects now support *index* and *count* methods:

```python
>>>
>>> range(0, 100, 2).count(10)
1
>>> range(0, 100, 2).index(10)
5
>>> range(0, 100, 2)[5]
10
>>> range(0, 100, 2)[0:5]
range(0, 10, 2)
```

- The ```datetime``` module has a new type ```timezone``` that implements the ```tzinfo``` 
interface by returning a fixed UTC offset and timezone name. This makes it easier to create 
timezone-aware datetime objects:

```python
>>>
>>> from datetime import datetime, timezone

>>> datetime.now(timezone.utc)
datetime.datetime(2010, 12, 8, 21, 4, 2, 923754, tzinfo=datetime.timezone.utc)

>>> datetime.strptime("01/01/2000 12:00 +0000", "%m/%d/%Y %H:%M %z")
datetime.datetime(2000, 1, 1, 12, 0, tzinfo=datetime.timezone.utc)
```

- Also, ```timedelta``` objects can now be multiplied by float and divided by float and int 
objects. And ```timedelta``` objects can now divide one another.

- The ```itertools``` module has a new ```accumulate()``` function modeled on APL’s scan operator 
and Numpy’s accumulate function:

```python
>>>
from itertools import accumulate
list(accumulate([8, 2, 50]))
[8, 10, 60]
>>>
prob_dist = [0.1, 0.4, 0.2, 0.3]
list(accumulate(prob_dist))      # cumulative probability distribution
[0.1, 0.5, 0.7, 1.0]
```

- The ```collections.Counter``` class now has two forms of in-place subtraction, the existing 
```-=``` operator for saturating subtraction and the new ```subtract()``` method for regular 
subtraction. 

```python
>>> from collections import Counter
>>> tally = Counter(dogs=5, cats=3)
>>> tally -= Counter(dogs=2, cats=8)    # saturating subtraction
>>> tally
Counter({'dogs': 3})
>>> tally = Counter(dogs=5, cats=3)
>>> tally.subtract(dogs=2, cats=8)      # regular subtraction
>>> tally
Counter({'dogs': 3, 'cats': -5})
```

- The ```collections.OrderedDict``` class has a new method ```move_to_end()``` which takes an 
existing key and moves it to either the first or last position in the ordered sequence.

```python
>>> from collections import OrderedDict
>>> d = OrderedDict.fromkeys(['a', 'b', 'X', 'd', 'e'])
>>> list(d)
['a', 'b', 'X', 'd', 'e']
>>> d.move_to_end('X')
>>> list(d)
['a', 'b', 'd', 'e', 'X']
```

- The ```abc``` module now supports ```abstractclassmethod()``` and ```abstractstaticmethod()```.
These tools make it possible to define an abstract base class that requires a particular 
```classmethod()``` or ```staticmethod()``` to be implemented:

```python
class Temperature(metaclass=abc.ABCMeta):
    @abc.abstractclassmethod
    def from_fahrenheit(cls, t):
        ...
    @abc.abstractclassmethod
    def from_celsius(cls, t):
        ...
```

[Python 3.2]: https://docs.python.org/3/whatsnew/3.2.html
[PEP 384]: https://www.python.org/dev/peps/pep-0384/
[PEP 389]: https://www.python.org/dev/peps/pep-0389/
[PEP 391]: https://www.python.org/dev/peps/pep-0391/