# What’s New In [Python 3.0]

## [PEP 3105] - Make print a function

```python
Old: print "The answer is", 2*2
New: print("The answer is", 2*2)

Old: print x,           # Trailing comma suppresses newline
New: print(x, end=" ")  # Appends a space instead of a newline

Old: print              # Prints a newline
New: print()            # You must call the function!

Old: print >>sys.stderr, "fatal error"
New: print("fatal error", file=sys.stderr)

Old: print (x, y)       # prints repr((x, y))
New: print((x, y))      # Not the same as print(x, y)!
```

## Views And Iterators Instead Of Lists

- ```dict``` methods ```dict.keys()```, ```dict.items()``` and ```dict.values()``` return *“[views]”*
instead of lists.
- ```dict.iterkeys()```, ```dict.iteritems()``` and ```dict.itervalues()``` methods are no longer 
supported.
- ```map()``` and ```filter()``` built-in functions return iterators.
- ```range()``` now behaves like ```xrange()```. The later no longer exists.
- ```zip()``` now returns an iterator.

## Simplified ordening comparison rules

- The ordering comparison operators (<, <=, >=, >) raise a ```TypeError``` exception when the 
operands don’t have a meaningful natural ordering.
- ```builtin.sorted()``` and ```list.sort()``` uses now the *key* argument providing a 
comparison function.
- The ```cmp()``` function should be treated as gone, and the ```__cmp__()``` special method is no 
longer supported. 

## Text vs. Data

- Python 3.0 uses the concepts of **text** and (binary) **data** instead of *Unicode* strings and 
*8-bit* strings.
- The type used to hold text is ```str```, the type used to hold data is ```bytes```. 
- You can no longer use ```u"..."``` literals for Unicode text. However, you must use ```b"..."``` 
literals for binary data.
- As the ```str``` and ```bytes``` types cannot be mixed, you must always explicitly convert between them:
  - Use ```str.encode()``` to go from ```str``` to ```bytes```.
  - Use ```bytes.decode()``` to go from ```bytes``` to ```str```.

## [PEP 3107]: Function argument and return value annotations.

```python

def foo(a: expression, b: expression = 5) -> expression:
    ... 
```

[Python 3.0]: https://docs.python.org/3/whatsnew/3.0.html
[PEP 3105]: https://www.python.org/dev/peps/pep-3105/
[views]: https://docs.python.org/3/library/stdtypes.html#dict-views
[PEP 3107]: https://www.python.org/dev/peps/pep-3107/