# What’s New In [Python 3.3]

## [PEP 405]: Virtual Environments

This PEP adds the ```venv``` module for programmatic access, and the ```pyvenv``` script for 
command-line access and administration. The Python interpreter checks for a ```pyvenv.cfg```, file 
whose existence signals the base of a virtual environment’s directory tree.

## [PEP 380]: Syntax for Delegating to a Subgenerator

This PEP adds the ```yield from``` expression, allowing a generator to delegate part of its 
operations to another generator. Additionally, the subgenerator is allowed to return with a value, 
and the value is made available to the delegating generator.

For simple iterators, ```yield from iterable``` is essentially just a shortened form of 
```for item in iterable: yield item```:

```python
>>>
>>> def g(x):
...     yield from range(x, 0, -1)
...     yield from range(x)
...
>>> list(g(5))
[5, 4, 3, 2, 1, 0, 1, 2, 3, 4]
```

## [PEP 409]: Suppressing exception context

This PEP introduces new syntax that allows the display of the chained exception context to be 
disabled. This allows cleaner error messages in applications that convert between exception types:

```python
>>>
>>> class D:
...     def __init__(self, extra):
...         self._extra_attributes = extra
...     def __getattr__(self, attr):
...         try:
...             return self._extra_attributes[attr]
...         except KeyError:
...             raise AttributeError(attr) from None
...
>>> D({}).x
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 8, in __getattr__
AttributeError: x
```

Without the ```from None``` suffix to suppress the cause, the original exception would be 
displayed by default:

```python
>>>
>>> class C:
...     def __init__(self, extra):
...         self._extra_attributes = extra
...     def __getattr__(self, attr):
...         try:
...             return self._extra_attributes[attr]
...         except KeyError:
...             raise AttributeError(attr)
...
>>> C({}).x
Traceback (most recent call last):
  File "<stdin>", line 6, in __getattr__
KeyError: 'x'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 8, in __getattr__
AttributeError: x
No debugging capability is lost, as the original exception con
```
## [PEP 3155]: Qualified name for classes and functions

Functions and class objects have a new ```__qualname__``` attribute representing the “path” from 
the module top-level to their definition. For global functions and classes, this is the same as 
```__name__```. For other functions and classes, it provides better information about where they 
were actually defined, and how they might be accessible from the global scope.

Example with (non-bound) methods:

```python
>>>
>>> class C:
...     def meth(self):
...         pass
>>> C.meth.__name__
'meth'
>>> C.meth.__qualname__
'C.meth'
```

Example with nested classes:

```python
>>>
>>> class C:
...     class D:
...         def meth(self):
...             pass
...
>>> C.D.__name__
'D'
>>> C.D.__qualname__
'C.D'
>>> C.D.meth.__name__
'meth'
>>> C.D.meth.__qualname__
'C.D.meth'
```

Example with nested functions:

```python
>>>
>>> def outer():
...     def inner():
...         pass
...     return inner
...
>>> outer().__name__
'inner'
>>> outer().__qualname__
'outer.<locals>.inner'
```

[Python 3.3]: https://docs.python.org/3/whatsnew/3.3.html
[PEP 405]: https://www.python.org/dev/peps/pep-0405/
[PEP 380]: https://www.python.org/dev/peps/pep-0380/
[PEP 409]: https://www.python.org/dev/peps/pep-0409/
[PEP 3155]: https://www.python.org/dev/peps/pep-3155