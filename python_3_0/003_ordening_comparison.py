# assert 1 < ''               # TypeError: '<' not supported between instances of 'int' and 'str'
# assert 0 > None             # TypeError: '>' not supported between instances of 'int' and 'NoneType'
# assert len <= len           # TypeError: '<=' not supported between instances of 'builtin_function_or_method' and 'builtin_function_or_method'
# assert None < None          # TypeError: '<' not supported between instances of 'NoneType' and 'NoneType'

assert 1 != ''
assert 0 != None
assert len == len
assert None == None

#
# builtin.sorted() and list.sort() doesn't support comparison function anymore
#
nums = [4, 8, 6, 5, 3, 2, 8, 9, 2, 5]
s = sorted(nums)
print(type(s), s)           # <class 'list'> [2, 2, 3, 4, 5, 5, 6, 8, 8, 9]

d = {1: 'D', 2: 'B', 3: 'B', 4: 'E', 5: 'A'}
s = sorted(d)
print(type(s), s)           # <class 'list'> [1, 2, 3, 4, 5]

text = 'This is a test from Andrew.'
s = sorted(text.split(), key=str.lower)
print(type(s), s)           # <class 'list'> ['a', 'Andrew.', 'from', 'is', 'test', 'This']

student_tuples = [
    ('john', 'A', 15),
    ('jane', 'B', 10),
    ('dave', 'B', 12),
]
s = sorted(student_tuples)
print(type(s), s)           # <class 'list'> [('dave', 'B', 10), ('jane', 'B', 12), ('john', 'A', 15)]

s = sorted(student_tuples, key=lambda student: student[2])
print(type(s), s)           # <class 'list'> [('jane', 'B', 10), ('dave', 'B', 12), ('john', 'A', 15)]

s = sorted(student_tuples, key=lambda student: student[0][1])
print(type(s), s)           # <class 'list'> [('jane', 'B', 12), ('dave', 'B', 10), ('john', 'A', 15)]

s = sorted(student_tuples, key=lambda student: student[2], reverse=True)
print(type(s), s)           # <class 'list'> [('john', 'A', 15), ('dave', 'B', 12), ('jane', 'B', 10)]

s = sorted(student_tuples, key=lambda student: student[0][1], reverse=True)
print(type(s), s)           # <class 'list'> [('john', 'A', 15), ('jane', 'B', 12), ('dave', 'B', 10)]