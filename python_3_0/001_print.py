#
# Default print() on STDOUT
#

print('Hello World!!!')                 # Hello World!!!
print(2*2)                              # 4
res = 2*2
print(res)                              # 4

#
# String concatenation
#

name = 'John Doe'
print('Hello ' + name + '!!!')          # Hello John Doe!!!
# print('The answer is ' + res)           # TypeError: can only concatenate str (not "int") to str
print('The answer is ' + str(res))      # The answer is 4
print('The answer is', res)             # The answer is 4
print('The answer is', res, sep=':')    # The answer is:4

num1 = 6
num2 = 2
mult = num1 * num2

res = mult % 2
if res == 0:
    even_odd = 'even'
else:
    even_odd = 'odd'

# The answer of 6 * 2 is 12 that is a even number.
print('The answer of', num1, '*', num2, 'is', mult, 'that is a', even_odd, 'number.')  
