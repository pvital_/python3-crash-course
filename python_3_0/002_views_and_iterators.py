#
# dict.keys(), dict.items() and dict.values() return “views”
#

d = {
    'first_name': 'John',
    'last_name': 'Doe',
}

keys = d.keys()
print(type(keys), keys)         # <class 'dict_keys'> dict_keys(['first_name', 'last_name'])

items = d.items()
print(type(items), items)       # <class 'dict_items'> dict_items([('first_name', 'John'), ('last_name', 'Doe')])

values = d.values()
print(type(values), values)     # <class 'dict_values'> dict_values(['John', 'Doe'])

#
# map(), filter() and zip() built-in functions return iterators.
#

str_nums = ["4", "8", "6", "5", "3", "2", "8", "9", "2", "5"]

int_nums = map(int, str_nums)
print(type(int_nums), int_nums)                     # <class 'map'> <map object at 0x105f07f10>
print(list(int_nums))                               # [4, 8, 6, 5, 3, 2, 8, 9, 2, 5]

print(list(map(str.upper, values)))                 # ['JOHN', 'DOE']
print('Hello', ' '.join(map(str.upper, values)))    # Hello JOHN DOE

int_nums = map(int, str_nums)
even_nums = filter(lambda x: x%2 == 0, list(int_nums))
print(type(even_nums), even_nums)                   # <class 'filter'> <filter object at 0x105157ee0>
print(list(even_nums))                              # [4, 8, 6, 2, 8, 2]

nums = [1, 2, 3]
letters = ['a', 'b', 'c']
zipped = zip(letters, nums)
print(type(zipped), zipped)                         # <class 'zip'> <zip object at 0x10517af80>
print(list(zipped))                                 # [('a', 1), ('b', 2), ('c', 3)]